# -*- mode: python ; coding: utf-8 -*-
import PyInstaller

datas = []
# spaCy
datas.extend(PyInstaller.utils.hooks.collect_data_files('spacy.lang', include_py_files = True))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('spacy_lookups_data'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('de_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('el_core_news_sm'))
datas.extend(PyInstaller.utils.hooks.collect_data_files('en_core_web_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('es_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('fr_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('it_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('lt_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('nb_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('nl_core_news_sm'))
#datas.extend(PyInstaller.utils.hooks.collect_data_files('pt_core_news_sm'))
datas.extend(PyInstaller.utils.hooks.collect_data_files('thinc'))

hiddenimports = [
    # pymorphy2
    'pymorphy2_dicts_ru',
    'pymorphy2_dicts_uk',

    # spaCy
    'spacy.kb',
    'spacy.lexeme',
    'spacy.matcher._schemas',
    'spacy.morphology',
    'spacy.parts_of_speech',
    'spacy.syntax._beam_utils',
    'spacy.syntax._parser_model',
    'spacy.syntax.arc_eager',
    'spacy.syntax.ner',
    'spacy.syntax.nn_parser',
    'spacy.syntax.stateclass',
    'spacy.syntax.transition_system',
    'spacy.tokens._retokenize',
    'spacy.tokens.morphanalysis',
    'spacy.tokens.underscore',

    'blis',
    'blis.py',

    'cymem',
    'cymem.cymem',

    'murmurhash',

    'preshed.maps',

    'srsly.msgpack.util',

    'thinc.extra.search',
    'thinc.linalg',
    'thinc.neural._aligned_alloc',
    'thinc.neural._custom_kernels',

    # spaCy models
    'de_core_news_sm',
    'el_core_news_sm',
    'en_core_web_sm',
    'es_core_news_sm',
    'fr_core_news_sm',
    'it_core_news_sm',
    'lt_core_news_sm',
    'nb_core_news_sm',
    'nl_core_news_sm',
    'pt_core_news_sm'
]

block_cipher = None


a = Analysis(['legal_case_reports_summarizer.py'],
             pathex=['D:\\Master\\Pravna informatika\\Projekat\\legal-document-summarization-nlp-project'],
             binaries=[],
             datas=datas,
             hiddenimports=hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='legal_case_reports_summarizer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True , icon='LegalIcon.ico')

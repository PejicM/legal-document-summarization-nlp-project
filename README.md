# Legal Document Summarization - NLP project

### Paper: open file "Sumarizacija pravnih dokumenata.pdf"

### Source Code: folder source -> files "summarizer.py" and "legal_case_reports_summarizer.py"
* Requirements: folder source -> "requirements.txt"
* use: pip install -r requirements.txt before running source code

### Data: folder corpus -> corpus -> fulltext_test (few documents in xml format) or fulltext (all documents in xml format) or plaintext (all txt documents)

### Executable file: dist -> legal_case_reports_summarizer.exe
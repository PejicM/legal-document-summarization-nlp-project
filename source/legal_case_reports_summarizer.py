# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 17:46:59 2020

@author: Marko Pejić
"""

#import tkinter        # nepotrebno, ali exe pravi problem bez ovog
#from tkinter import filedialog    # isto kao i prethodni komentar
import sys
import re
import summarizer
from PyQt5.QtWidgets import QMainWindow,QButtonGroup, QApplication, QRadioButton, QSlider, QWidget, QPushButton, QAction, QTextEdit, QLineEdit, QMessageBox, QLabel, QFileDialog
from PyQt5 import QtGui
from PyQt5.QtGui import QIcon, QFont, QSyntaxHighlighter
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot,Qt

import warnings
import matplotlib.cbook
warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)

class LCRSapp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Legal Case Reports Summarizer'
        self.left = 50
        self.top = 50
        self.width = 1920
        self.height = 1080
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        #self.showFullScreen()
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        label_font = QFont('SansSerif', 14, QFont.Bold)
        button_font = QFont('SansSerif', 10)
        bold_font = QFont('SansSerif', 10, QFont.Bold)
        slider_font = QFont('SansSerif', 8, QFont.Bold)
        
        # Case report label
        self.label1 = QLabel(self)
        self.label1.move(20, 20)
        self.label1.resize(300, 50)
        self.label1.setText('Case report:')       
        self.label1.setFont(label_font)
        
        # Summary label
        self.label2 = QLabel(self)
        self.label2.move(1100, 20)
        self.label2.resize(300, 50)
        self.label2.setText('Summary:')       
        self.label2.setFont(label_font)
        
        # Create textbox for raw legal case report
        self.case_report_textbox = QTextEdit(self)
        self.case_report_textbox.move(20, 100)
        self.case_report_textbox.resize(800, 800)
        #self.case_report.setReadOnly(True)
            
        # Create textbox for legal case report summary
        self.summary_textbox = QTextEdit(self)
        self.summary_textbox.move(1100, 100)
        self.summary_textbox.resize(800, 800)
        self.summary_textbox.setReadOnly(True)
        
        # Create a load button
        self.load_button = QPushButton('Load Report', self)
        self.load_button.move(20, 950)
        self.load_button.resize(200, 50)
        self.load_button.setFont(button_font)
        
        # Create a summarize button
        self.summarize_button = QPushButton('Summarize Report', self)
        self.summarize_button.move(1700, 950)
        self.summarize_button.resize(200, 50)
        self.summarize_button.setFont(button_font)
        
        # Importancy label
        self.slider_label = QLabel(self)
        self.slider_label.move(849, 100)
        self.slider_label.resize(300, 50)
        self.slider_label.setText('Importancy configuration')       
        self.slider_label.setFont(bold_font)
        
        # Date label
        self.date_label = QLabel(self)
        self.date_label.move(940, 160)
        self.date_label.resize(300, 50)
        self.date_label.setText('Date(%)')       
        self.date_label.setFont(slider_font)
        
        # Date slider
        self.date_slider = QSlider(Qt.Horizontal, self)
        self.date_slider.setMinimum(0)
        self.date_slider.setMaximum(10)
        self.date_slider.setValue(5)
        self.date_slider.setTickPosition(QSlider.TicksBelow)
        self.date_slider.setTickInterval(1)
        self.date_slider.setSingleStep(1) 
        self.date_slider.setPageStep(1)
        self.date_slider.setGeometry(850, 200, 220, 30)
        
        # People label
        self.people_org_label = QLabel(self)
        self.people_org_label.move(915, 260)
        self.people_org_label.resize(300, 50)
        self.people_org_label.setText('People & Org(%)')       
        self.people_org_label.setFont(slider_font)
        
        # People slider
        self.people_org_slider = QSlider(Qt.Horizontal, self)
        self.people_org_slider.setMinimum(0)
        self.people_org_slider.setMaximum(10)
        self.people_org_slider.setValue(5)
        self.people_org_slider.setTickPosition(QSlider.TicksBelow)
        self.people_org_slider.setTickInterval(1)
        self.people_org_slider.setSingleStep(1) 
        self.people_org_slider.setPageStep(1)
        self.people_org_slider.setGeometry(850, 300, 220, 30)
        
        # Entities label
        self.entities_label = QLabel(self)
        self.entities_label.move(930, 360)
        self.entities_label.resize(300, 50)
        self.entities_label.setText('Entities(%)')       
        self.entities_label.setFont(slider_font)
        
        # Entities slider
        self.entities_slider = QSlider(Qt.Horizontal, self)
        self.entities_slider.setMinimum(0)
        self.entities_slider.setMaximum(10)
        self.entities_slider.setValue(5)
        self.entities_slider.setTickPosition(QSlider.TicksBelow)
        self.entities_slider.setTickInterval(1)
        self.entities_slider.setSingleStep(1) 
        self.entities_slider.setPageStep(1)
        self.entities_slider.setGeometry(850, 400, 220, 30)
        
        # Summarization length label
        self.length_label = QLabel(self)
        self.length_label.move(893, 460)
        self.length_label.resize(300, 50)
        self.length_label.setText('Number of sentences')       
        self.length_label.setFont(slider_font)
        
        # Summarization length slider
        self.length_slider = QSlider(Qt.Horizontal, self)
        self.length_slider.setMinimum(1)
        self.length_slider.setMaximum(3)
        self.length_slider.setValue(2)
        self.length_slider.setTickPosition(QSlider.TicksBelow)
        self.length_slider.setTickInterval(1)
        self.length_slider.setSingleStep(1) 
        self.length_slider.setPageStep(1)
        self.length_slider.setGeometry(850, 500, 220, 30)
        
        # Keywords label
        self.keywords_label = QLabel(self)
        self.keywords_label.move(920, 560)
        self.keywords_label.resize(300, 50)
        self.keywords_label.setText('Keyphrases:')       
        self.keywords_label.setFont(slider_font)
        
        # Keywords textbox
        self.keywords_textbox = QTextEdit(self)
        self.keywords_textbox.move(850, 610)
        self.keywords_textbox.resize(220, 100)
        
        # Keyphrases importancy slider
        self.keywords_slider = QSlider(Qt.Horizontal, self)
        self.keywords_slider.setMinimum(0)
        self.keywords_slider.setMaximum(10)
        self.keywords_slider.setValue(5)
        self.keywords_slider.setTickPosition(QSlider.TicksBelow)
        self.keywords_slider.setTickInterval(1)
        self.keywords_slider.setSingleStep(1) 
        self.keywords_slider.setPageStep(1)
        self.keywords_slider.setGeometry(850, 720, 220, 30)
        
        # Chronological radio button
        self.chr_rb = QRadioButton('Chronological', self)
        self.chr_rb.setChecked(True)
        self.chr_rb.setGeometry(QtCore.QRect(1100, 80, 100, 21))
        self.chr_rb.setObjectName("newButton")
        
        # Importancy radio button
        self.imp_rb = QRadioButton('Importancy', self)
        self.imp_rb.setGeometry(QtCore.QRect(1230, 80, 100, 21))
        self.imp_rb.setObjectName("newButton")
    
        # connect load_button to function file_open
        self.load_button.clicked.connect(self.file_open)
        
        # connect summarize_button to function summarize_text
        self.summarize_button.clicked.connect(self.summarize_text)
        
        self.show()
        
    def initialize_coefficients(self):
        self.summary_textbox.setText('')
        self.date_slider.setValue(5)
        self.people_org_slider.setValue(5)
        self.entities_slider.setValue(5)
        self.length_slider.setValue(2)
        self.keywords_textbox.setText('')
        self.keywords_slider.setValue(5)
        self.chr_rb.setChecked(True)
        
    def highlight_sentence(self, sentence, color):
        sentence = sentence.replace(")","\)")
        sentence = sentence.replace("(","\(")
        sentence = sentence.replace("]","\]")
        sentence = sentence.replace("[","\[")
        sentence = sentence.replace(".","\.")
        sentence = sentence.replace("}","\}")
        sentence = sentence.replace("{","\{")
        sentence = sentence.replace("^","\^")
        sentence = sentence.replace("*","\*")
        sentence = sentence.replace("+","\+")
        sentence = sentence.replace("?","\?")
        sentence = sentence.replace("$","\$")
        sentence = sentence.replace("|","\|")
        cursor = self.case_report_textbox.textCursor()
        # Setup the desired format for matches
        format = QtGui.QTextCharFormat()
        highlight_color = QtGui.QColor(color)
        if(color == 'red' or color == 'blue'):
            highlight_color.setAlpha(128)
        format.setBackground(QtGui.QBrush(highlight_color))
        # Setup the regex engine
        regex = QtCore.QRegExp(sentence)
        # Process the displayed document
        pos = 0
        index = regex.indexIn(self.case_report_textbox.toPlainText(), pos)
        
        while (index != -1):
            # Select the matched text and apply the desired format
            cursor.setPosition(index)
            cursor.movePosition(QtGui.QTextCursor.EndOfBlock, 1)
            cursor.mergeCharFormat(format)
            # Move to the next match
            pos = index + regex.matchedLength()
            index = regex.indexIn(self.case_report_textbox.toPlainText(), pos)
            
    def clear_highlight(self):
        cursor = self.case_report_textbox.textCursor()
        cursor.setPosition(0)
        # Setup the desired format for matches
        format = QtGui.QTextCharFormat()
        format.setBackground(QtGui.QBrush(QtGui.QColor('white')))
        cursor.movePosition(QtGui.QTextCursor.End, 1)
        cursor.mergeCharFormat(format)
        
    @pyqtSlot()
    def file_open(self):
        name, _ = QFileDialog.getOpenFileName(self, 'Open File')
        with open(name, 'r') as file:
            text = file.read()
            self.file_path = name
        file.closed
        
        if(name.endswith('.xml')):
            data = []
            text = text.strip().replace('\n', '')
            sentences = re.findall(r'<sentence .*?>(.*?)</sentence>+', text)
            name = re.findall(r'<name>(.*?)</name>+', text)[0]
            data.append([name, sentences])
            text = ''
            data = []
            data.append(name)
    
            for sent in sentences:
                if(len(sent) > 10):
                    data.append(sent)
                text += sent + '\n'
        else:
            data = summarizer.split_plaintext(text)
            text = '\n'.join(data)
        
        self.initialize_coefficients()
        self.clear_highlight()
        self.case_report_textbox.setText(text)
        
    @pyqtSlot()
    def summarize_text(self):
        self.clear_highlight()
        coefficients = (self.date_slider.value() / 10, self.people_org_slider.value() / 10, self.entities_slider.value() / 10, self.keywords_slider.value() / 10)
        sentence_num = self.length_slider.value() * 5
        keyphrases = self.keywords_textbox.toPlainText().lower().split('\n')
        
        summary_pairs = summarizer.summarization(self.file_path, self.file_path, coefficients, sentence_num, self.chr_rb.isChecked(), keyphrases)
        summary = [pair[0] for pair in summary_pairs]
        scores = [pair[1] for pair in summary_pairs]
        max_score = max(scores)
        min_score = min(scores)
        colors = ['blue', 'yellow', 'orange', 'red']
        step = (max_score - min_score) / (len(colors) - 1)
        
        self.summary_textbox.setText('\n\n'.join(summary))

        print('{} - {}'.format(min_score, max_score))

        for i in range(0, sentence_num):
            sentence = summary_pairs[i][0]
            score = summary_pairs[i][1]
                
            for col in range(0, len(colors)):
                bottom = min_score + col * step
                top = bottom + step
                #print('{} - {}'.format(bottom, top))
                
                if((score >= bottom) and (score <= top)):
                    self.highlight_sentence(sentence, colors[col])
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('LegalIcon.ico'))
    ex = LCRSapp()
    sys.exit(app.exec_())
    

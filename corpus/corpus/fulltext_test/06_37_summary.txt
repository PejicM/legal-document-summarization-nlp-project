 1 On 20 October 2005, the primary Judge (Marshall J) dismissed an application by the applicant pursuant to s 179 of the Bankruptcy Act 1966 (Cth) ('the Act') for the removal of the respondent as his trustee in bankruptcy and ordered that the applicant pay the respondent's costs of and incidental to the application: see Vasiliou v Marchesi [2005] FCA 1471.
2 As the orders were made on the basis of all of the material relied upon by the applicant, it is appropriate to treat them as final orders with the consequence that any appeal was to be filed within 21 days: see O 52 r 15(1) of the Federal Court Rules .
Rather, on 28 November 2005 he applied for leave to appeal out of time.
Under O 52 r 15(2) the Court or a Judge 'for special reasons' may give leave to file and serve a notice of appeal out of time.
4 In Gallo v Dawson [1990] HCA 30 ; (1990) 93 ALR 479 at 480, McHugh J, citing Hughes v National Trustees Executors &amp; Agency Co of Australia Ltd [1978] VR 257 at 262 observed that the discretion to extend time for an appeal was for 'the sole purpose of enabling the court [or a judge] to do justice between the parties' and stated, in that context, it is always necessary to consider the prospects of success.
The Court has not allow the applicant an adjournment to obtain further evidence required.
The Court has ignored in principal the affidavit sworn by the applicant.
Compensation for loss  3.
And any other remedy the Court will allow.
It is well established that the Court will not initiate an inquiry under s 179 unless it is satisfied that a proper case for an inquiry has been demonstrated; see Wilson v Commonwealth of Australia [1999] FCA 219 at [44] and the cases cited therein.
See also Turner v Official Trustee in Bankruptcy , unreported Full Court, 27 November 1998 as cited in Macchia v Nilant [2001] FCA 7 ; (2001) 110 FCR 101 at 120.
" 7 Section 179(1)(a) of the Act empowers the Court, inter alia, on the application of a bankrupt to inquire into the conduct of a trustee and to remove the trustee from office and make such order as it thinks proper.
10 The basis on which the application was dismissed by the primary judge was that the material filed by the applicant contained allegations and assertions, rather than relevant and admissible evidence upon which it was appropriate for the Court to act.
13 The application of the applicant for leave to appeal out of time is to be dismissed with costs.
Associate:Dated: 9 February 2006 For the Applicant: The Applicant appeared in person   Solicitor appearing for the Respondent: M Lhuede   Solicitor for the Respondent: Piper Alderman   Date of Hearing: 3 February 2006   Date of Judgment: 3 February 2006    AustLII: Copyright Policy | Disclaimers | Privacy Policy | Feedback  URL: http://www.austlii.edu.au/au/cases/cth/FCA/2006/37.html   
